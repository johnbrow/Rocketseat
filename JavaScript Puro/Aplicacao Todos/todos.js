// percorredno a tag 'ul' com o query selector na div app
var listElement = document.querySelector('#app ul');

// percorredno a tag 'input'com o query selector na div app
var inputElement = document.querySelector('#app input');

// percorredno a tag 'button' com o query selector na div app
var buttonElement = document.querySelector('#app button');


// extinto array que estava sendo ultilizado para adicionar dados a nossa lista
// var todos = [
//     'Fazer Café',
//     'Estudar JavaScript',
//     'Acessar Comunidade da Rocketseat'
// ];


// agora os dados estao no localStorage sendo buscados por aqui  
var todos = JSON.parse(localStorage.getItem('listTodos')) || [] ;
// e se nao tiver nenhum dado no LocalStorage ele manda um array vazio


// uma function criada para renderizar os dados do array todos
function renderTodos() {

    // para esvaziar todos os valores das tags 'li' no html
    listElement.innerHTML = '';

    // esse "for of" e uma funcao para armezenar os dados separadamente em sua lista
    for (todo of todos) {

        // variavel criando a tag 'li'
        var todoElement = document.createElement('li');

        // variavel capturadno o texto na variavel todo, ela que separa cada texto em sua posiçao
        var todoText = document.createTextNode(todo);

        var linkElement = document.createElement('a');
        linkElement.setAttribute('href', '#');

        // capturando a posicao de cada elemento com 'indexOf()' 
        var pos = todos.indexOf(todo);
        // pegando a posicao do excluir clicado que coisa bonita pqp S2
        linkElement.setAttribute('onclick', 'deleteTodo(' + pos + ')');


        var linkText = document.createTextNode(' Excluir');

        linkElement.appendChild(linkText);

        //tag "li" sendo pai de todoText
        todoElement.appendChild(todoText);
        todoElement.appendChild(linkElement);

        // tag li sendo implementada na lista ul
        listElement.appendChild(todoElement);
    }
};

renderTodos();


// funcao para acrenscetamos o novor valor no array
function addtodo() {

    // captura o valor digitado no input
    var todoValor = inputElement.value;

    // o push serve para adicionar um novo valor dentro do array e depois colocamos o novo
    // valor digitado entre parenteses
    todos.push(todoValor);

    // e agora temos que deixar o input vazio antes de acrescentar um novo valor 
    inputElement.value = '';

    renderTodos();
    saveStorage();
};

// e eu so posso chamar a funcao addTodo atraves do click do botao
buttonElement.onclick = addtodo;

// funcao para deletar o elemento li clicado e capturando a sua posiçao
function deleteTodo(pos) {
    todos.splice(pos, 1);
    renderTodos();
    saveStorage();
};

// funcao para salvar dados local no navegador e tals kkk
function saveStorage() {

    // LocalStorage essa variavel global monstruosa faz isso pra nois
    localStorage.setItem('listTodos',JSON.stringify(todos) );
    // so que ela nao manda array e nem vetor entao vamos ter que converter para JSON
};