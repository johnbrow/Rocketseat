
//variavel que retorna uma funcao com resolve e reject de uma promise

var minhaPromise = function () {
    return new Promise(function (resolve, reject) {

        var xhr = new XMLHttpRequest();

        xhr.open('GET', 'https://api.github.com/users/Johnbrow10');
        xhr.send(null);

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                }
                else {
                    reject('Erro na requisição');
                }
            }

        }

    });
}

// verificando a funcao minhaPromise,para que a promise seja carregada com escopo cheio 
minhaPromise()
    .then(function (response) { //funcao que chama o resolve da promise
        console.log(response);
    })
    .catch(function (error) {    //funcao que chama o reject
        console.warn(error);
    });

