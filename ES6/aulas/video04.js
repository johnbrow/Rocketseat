// CLASSES E CONSTRUTORES 04

class List {
    constructor (){
        this.data = [];
    }

    add(data) {
        this.data.push(data);
        console.log(this.data)
    }
}

class Todolist extends List {
    constructor (){
        super();
        this.usuario = 'JohnBrow';
    }
    mostrarUsuario(){
        console.log(this.usuario);
    }
}

const MinhaLista = new Todolist();

document.getElementById('novotodo').onclick = function(){
    MinhaLista.add('novo Todo');
}


// METODO ESTATICO E O CARA QUE E O FODA SE PRO CONSTRUTOR E PRO RESTO DA CLASSE

class Matematica {
    constructor (){
        this.todos = [];
    }

    static soma(a,b){
        return a + b;

    }
}

console.log(Matematica.soma(1,3));