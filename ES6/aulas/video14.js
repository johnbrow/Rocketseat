//USANDO EXPORTACAO DE UMA FUNCAO DO FUNCOES PARA OUTRO ARQUIVO

export function soma(a ,b){
    return a * (b + a)
};

export function sub(a, b){
    return a - b;
};

export function mult(a, b){
    return a * b;
};

//AGORA IMPORTANDO PARA O MAIN.JS
import {soma,sub,mult} from './funcoes';

console.log(soma(7,4));
console.log(sub(7,4));
console.log(mult(7,4));

//CADA ARQUIVO PODE TER UMA EXPORT DEFAULT E VARIAS EXPORTS
export default function divi(a, c){
    return  a/c;
};

//AI NA EXPORTACAO POSSO CHAMAR O NOME DO ARQUIVO E RENOMEAR O NOME DA FUNCAO DA FORMA QUE QUEREMOS
import diviExport from './divi';