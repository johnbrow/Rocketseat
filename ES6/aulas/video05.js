// CONSTANTES E LET 05
// ALTERAR UMA CONSTANTE

// ERRADO
const a = 1;
a = 3;

// CERTO, COM MUTACAO DE VARIAVEL
const usuario = { nome: 'john' };

usuario.nome = 'Kratos';

function teste(x) {
    let y = 3;

    if (x > 4) {
        console.log(x + 6);
    }
}
teste(10);

// OPERACAO EM VETORES O6