//REST
//em objetos
const usuario = {
    nome: 'snoppy',
    idade: 13,
    cor:'malhado'
};

const {nome,...resto} = usuario;
console.log(nome);
console.log(resto);

//em arrays

const arr = [1,2,3,4,5,6];
const [a,b,...c] = arr;
console.log(a);
console.log(b);
console.log(c);

//em parametros de funções

function soma (...params){
    return params.reduce((total,next)=>total+next);
}

console.log(soma(1,3,5));


//SPREAD

//junção de arrays
const arr1 = [1,2,3,7];
const arr2 = [4,5,6,0];

const arr3 = [...arr1, ...arr2];
console.log(arr3);

//com objetos
const usuario1 = {
    nome: 'snoopy',
    idade: 13,
    olhos: 'castanho',
};

const usuario2 = {... usuario1,nome:'bruce'};
console.log(usuario2);