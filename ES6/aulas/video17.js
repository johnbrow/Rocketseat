// aula focada na ultilizacao da biblioteca axios(gerador de https para se conectar a uma api ou criar uma)
// e tbm ultilizando o async para verificar se a requisiçao foi perfeita  
import axios from 'axios';

class Api {
    static async getUserInfo(username) { 
        try{
            const response = await axios.get(`https://api.github.com/users/${username}`)
            console.log(response);
        }
       catch(err){
           console.warn('Erro na Api')
       }
    }
}

Api.getUserInfo('johnbrow100');
Api.getUserInfo('johnbrow10');