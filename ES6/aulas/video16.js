const minhaPromise = () => new Promise((resolve, reject) => {
    setTimeout(() => { resolve('ok') }, 2000);
});

//essa promise simples sempre tem que ter o .then e .catch para ser executado.
minhaPromise()
    .then(response => {
        console.log(response);
    })
    .catch(err => {
        console.error('Deu merda me patrao');
    });

// agora com o pika async_await(um plugin dentro do @babel) ele ultiliza uma simples funcao.
async function executaPromise() {
    console.log(await minhaPromise());
    console.log(await minhaPromise());
    console.log(await minhaPromise());
}
// com o await na frente o minhaPromise e execultado apenas depois de terminar o processo da linha acima. 
executaPromise(); 
