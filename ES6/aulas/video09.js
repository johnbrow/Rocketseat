const usuario = {
    nome: 'snoopy',
    idade: 13,
    endereco: {
        cidade: 'São paulo',
        estado: 'SP'
    },
};

// BUSCAR INFORMAÇOES DO OBJETO EM JS

// const nome = usuario.nome;
// const idade = usuario.idade;
// const cidade = usuario.endereco.cidade;


// AGORA A DESUSTRUTURACAO DO ES6 CONVERTEMOS ASSIM UM OBJETO

const { nome, idade, endereco: { cidade } } = usuario;
console.log(nome);
console.log(idade);
console.log(cidade);

// TAMBEM DA PRA SE USAR DENTRO DE UMA FUNCAO
function mostrarNome({ idade, nome }) {
    console.log(nome, idade);
}
mostrarNome(usuario);