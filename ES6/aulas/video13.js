//PREPARANDO O AMBIENTE PARA USAR O WEBPACK CONFIG
module.exports = {
    entry: './main.js',
    output: {
        path: __dirname,
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            }
        ],
    },
}

//E DEPOIS TEVE UMAS ALTERACOES NO PACKAGE PARA RODAR O WEBPACK, ULTILIZANDO O YARN DEV

//"dev": "webpack --mode=development -w"

//COM WEBPACK PODEMOS ULTILIZAR MAIS DE UMA PAGINA JS NO PROJETO EXPOETANDO E IMPORTAANDO NO JS PRINCIPAL 

//MAIN.JS
import { soma } from './funcoes';
console.log(soma(7, 4))

//FUNCOES.JS
export function soma(a, b) {
    return a * (b + a)
};