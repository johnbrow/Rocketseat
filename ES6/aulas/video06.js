const arr = [1, 3, 4, 5, 8, 9];

//O 'MAP' VERIFICA A POSICAO DE CADA INTEM DO ARRAY
const newArr = arr.map(function (item, index) {
    return item + index;
});
console.log(newArr);


const reduce = arr.reduce(function (total, next) {
    return total + next;
});
console.log(reduce);


const filter = arr.filter(function (item) {
    return item % 2 === 0;
});
console.log(filter);

const find = arr.find(function (item) {
    return item === 4;
});

console.log(find);
