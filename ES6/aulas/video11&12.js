//TEMPLATE LI, SUBSTITUINDO A CONCATENAÇÃO VIDEO 11
const nome = 'Johnbrow';
const idade = 19;

// COM A CONCATENAÇAO CLASSICA

//console.log("meu nome é " + nome + " e tenho " + idade + " anos.");

//AGORA COM O TEMPLATE LI DO ES6
console.log(`meu nome é ${nome} e tenho ${idade} anos`);



//SINTAX CURTA DO ES6 VIDEO 12

const nome1 = 'jinx';
const idade1 = 12;

var usuario = {
    nome1,
    idade1,
    cabelo: 'laranja',
};


console.log(usuario);