const arr = [1, 3, 4, 5, 6];

// MODO PLEBEU DE SE USAR O ARROW FUNCTION

// const newArr = arr.map(function(item) {
//     return item *2;
// })


// MODO GOD OF ARROW FUNCTION DA PORRA TODA ,E QUANDO A FUNCAO E ANONIMA E SO TEM UM PARAMETRO

const newArr = arr.map(item => item * 2);
console.log(newArr);

// CRIANDO FUNCOES COM CONST PARA DEIXAR CURTA
// SO NAO RETORNA OBJETO, PQ AS CHAVES E CRIACAO DO ESCOPO 
const teste = () => 'teste';

//MAS SE COLOCAR ENTRE PARENTESES RESOLVE O ERRO
const teste1 = () => ({nome:'diego'});

console.log(teste());